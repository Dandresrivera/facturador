<?php

use App\Models\Tenant\City;
use App\Models\Tenant\Department;
use Illuminate\Database\Migrations\Migration;

class UpdateCitiesTable extends Migration
{
    protected $createdAt = '2020-12-23 10:00:00';
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        $departmentsCount = Department::query()->count();
        if ($departmentsCount !== 0) {
            City::updateCities($this->createdAt);
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        City::where('created_at', $this->createdAt)->forceDelete();
	}
}
