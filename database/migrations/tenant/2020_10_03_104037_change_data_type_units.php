<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDataTypeUnits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('type_units')->where('id', 1)->update(['deleted_at' => '2020-10-03 01:06:13']);
        DB::table('type_units')->where('id', 3)->update(['deleted_at' => '2020-10-03 01:06:13']);
        DB::table('type_units')->where('id', 4)->update(['deleted_at' => '2020-10-03 01:06:13']);

        DB::table('type_units')->where('id', 5)->update(['deleted_at' => '2020-10-03 01:06:13']);
        DB::table('type_units')->where('id', 6)->update(['deleted_at' => '2020-10-03 01:06:13']);
        DB::table('type_units')->where('id', 7)->update(['deleted_at' => '2020-10-03 01:06:13']);

        DB::table('type_units')->where('id', 8)->update(['deleted_at' => '2020-10-03 01:06:13']);
        DB::table('type_units')->where('id', 9)->update(['deleted_at' => '2020-10-03 01:06:13']);
        DB::table('type_units')->where('id', 11)->update(['deleted_at' => '2020-10-03 01:06:13']);
        
        DB::table('type_units')->where('id', 12)->update(['deleted_at' => '2020-10-03 01:06:13']);

        DB::table('type_units')->where('id', 2)->update(['code' => '479']);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
