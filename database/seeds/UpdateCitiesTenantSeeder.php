<?php

use App\Models\Tenant\City;
use Illuminate\Database\Seeder;

class UpdateCitiesTenantSeeder extends Seeder
{
    protected $createdAt = '2020-12-23 10:00:00';
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		City::updateCities($this->createdAt);
	}
}
