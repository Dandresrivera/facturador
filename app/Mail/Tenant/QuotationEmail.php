<?php

namespace App\Mail\Tenant;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Mail\Mailable;
use Illuminate\Bus\Queueable;
use App\Models\Tenant\{
    TypeIdentityDocument,
    Company,
    Quotation

};

use App\Models\TenantService\{
    Company as TenantServiceCompany
};
use PDF;



class QuotationEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Company
     * @var \App\Models\Tenant\Company
     */
    public $company;

    /**
     * Document
     * @var \App\Models\Tenant\Document
     */
    public $document;

    /**
     * TenantServiceCompany
     * @var \App\Models\TenantService\Company
     */
    public $servicecompany;



    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Company $company, Quotation $document, TenantServiceCompany $servicecompany) {
        $this->company = $company;
        $this->document = $document;
        $this->servicecompany = $servicecompany;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {

        $typeIdentityDocuments = TypeIdentityDocument::all();
        $company = $this->company;
        $document = $this->document;
        $servicecompany = $this->servicecompany;

        $pdf = PDF::loadView("pdf/quotation", compact("typeIdentityDocuments", "company", "servicecompany", "document"))->setWarnings(false);

        return $this->from($this->company->email, $this->company->name)
        ->subject("{$this->company->name} - Cotizacion {$this->document->id}")
        ->view('emails.tenant.quotation')
        ->attachData($pdf->output(), "COT-{$this->document->id}.pdf");
    }
}
