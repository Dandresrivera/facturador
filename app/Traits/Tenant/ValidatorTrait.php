<?php

namespace App\Traits\Tenant;

use Illuminate\Support\Facades\Log;

trait ValidatorTrait
{ 

    public function validateErrorsResponseApi($response_model)
    {
        
        //validation form request

        if(isset($response_model->errors)){

            $message = "";

            foreach ((array) $response_model->errors as $value) {
                $message = $message.' - '.implode(' - ', $value);
            }

            return $this->getErrorMessage($message);
            
        }

        //validation exception

        if(isset($response_model->exception)){

            $this->setErrorLog($response_model->line, $response_model->message, $response_model->file);

            return $this->getErrorMessage("{$response_model->exception} - {$response_model->message}");
            
        }

        return [
            'success' => true,
            'message' => '' 
        ];

    }


    public function getErrorMessage($message) {

        return [
            'success' => false,
            'message' => $message
        ];

    } 


    public function setErrorLog($line, $message, $file)
    {
        Log::error("Line: {$line} - Message: {$message} - File: {$file}");
    }

}
