<?php

namespace App\Http\Controllers\Tenant;

use App\Helpers\PDFHelper;
use Facades\App\Models\Tenant\Document as FacadeDocument;
use App\Traits\Tenant\DocumentTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\Tenant\{
    QuotationUpdateRequest,
    QuotationRequest
};
use Illuminate\Http\Request;
use App\Models\Tenant\{
    TypeIdentityDocument,
    DetailQuotation,
    DetailDocument,
    TypeDocument,
    TypeInvoice,
    Quotation,
    Currency,
    Document,
    Company,
    Client,
    Item,
    Tax
};
use Carbon\Carbon;
use Mpdf\Mpdf;
use DB;
use App\Models\TenantService\{
    Company as TenantServiceCompany
};

use App\Models\TenantService\{
    Company as ServiceTenantCompany
};
use PDF;

use Illuminate\Support\Facades\Mail;
use App\Mail\Tenant\QuotationEmail;
use App\Traits\Tenant\ValidatorTrait;


class QuotationController extends Controller
{
    use DocumentTrait, ValidatorTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('quotation.tenant.index');
    }

    public function create() {
        return view('quotation.tenant.create');
    }

    public function records()
    {
        return response()->json([
            'documents' => Quotation::with('state_quote', 'currency', 'detail_quotations')
                ->orderBy('id', 'desc')
                ->paginate(20),
        ], 200);
    }

    public function define()
    {
        return [

        ];
    }

    /**
     * All
     * @return \Illuminate\Http\Response
     */
    public function all() {
        return [
            'typeDocuments' => TypeDocument::all(),
            'typeInvoices' => TypeInvoice::all(),

            'currencies' => Currency::all(),
            'clients' => Client::all(),
            'items'  => Item::query()
                ->with('typeUnit', 'tax')
                ->get(),
            'taxes' => Tax::all()
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Tenant\QuotationRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(QuotationRequest $request) {
        DB::connection('tenant')->beginTransaction();

        try {
            $company = Company::firstOrFail();

            $quotation = Quotation::create([
                'client_id' => $request->client_id,
                'client' => Client::findOrFail($request->client_id),
                'currency_id' => $request->currency_id,
                'observation' => $request->observation,
                'sale' => $request->sale,
                'total_discount' => $request->total_discount,
                'taxes' => $request->taxes,
                'total_tax' => $request->total_tax,
                'subtotal' => $request->subtotal,
                'total' => $request->total,
                'version_ubl_id' => $company->version_ubl_id,
                'ambient_id' => $company->ambient_id,
                'seller' => $request->seller
            ]);

            foreach ($request->items as $item) {
                DetailQuotation::create([
                    'quotation_id' => $quotation->id,
                    'item_id' => $item['id'],
                    'item' => $item,
                    'type_unit_id' => $item['type_unit_id'],
                    'quantity' => $item['quantity'],
                    'price' => $item['price'],
                    'tax_id' => $item['tax_id'],
                    'tax' => Tax::find($item['tax_id']),
                    'total_tax' => $item['total_tax'],
                    'subtotal' => $item['subtotal'],
                    'discount' => $item['discount'],
                    'total' => $item['total']
                ]);
            }
        }
        catch (\Exception $e) {
            DB::connection('tenant')->rollBack();

            return [
                'success' => false,
                'message' => $e->getMessage()
            ];
        }

        DB::connection('tenant')->commit();

        return [
            'success' => true,
            'message' => "Se registro con éxito la cotización #{$quotation->id}."
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Tenant\Request  $request
     * @param  \App\Models\Tenant\Quotation  $quotation
     * @return \Illuminate\Http\Response
     */
    public function update(QuotationUpdateRequest $request, Quotation $quotation) {

        DB::connection('tenant')->beginTransaction();

        try {
            $company = Company::firstOrFail();

            $quotation->update([
                'client_id' => $request->client_id,
                'client' => Client::findOrFail($request->client_id),
                'currency_id' => $request->currency_id,
                'sale' => $request->sale,
                'total_discount' => $request->total_discount,
                'taxes' => $request->taxes,
                'total_tax' => $request->total_tax,
                'subtotal' => $request->subtotal,
                'total' => $request->total,
                'version_ubl_id' => $company->version_ubl_id,
                'ambient_id' => $company->ambient_id,
                'observation' => $request->observation,
                'seller' => $request->seller,
            ]);

            $quotation->detail_quotations()->delete();

            foreach ($request->items as $item) {
                DetailQuotation::create([
                    'quotation_id' => $quotation->id,
                    'item_id' => $item['id'],
                    'item' => $item,
                    'type_unit_id' => $item['type_unit_id'],
                    'quantity' => $item['quantity'],
                    'price' => $item['price'],
                    'tax_id' => $item['tax_id'],
                    'tax' => Tax::find($item['tax_id']),
                    'total_tax' => $item['total_tax'],
                    'subtotal' => $item['subtotal'],
                    'discount' => $item['discount'],
                    'total' => $item['total']
                ]);
            }

            DB::connection('tenant')->commit();

        }
        catch (\Exception $e) {
            DB::connection('tenant')->rollBack();

            return [
                'success' => false,
                'message' => $e->getMessage()
            ];
        }

        DB::connection('tenant')->commit();

        return [
            'success' => true,
            'message' => "Se actualizo con éxito la cotización #{$quotation->id}."
        ];
    }

    /**
     * To bill
     * @param  \App\Models\Tenant\Quotation $quotation
     * @return \Illuminate\Http\Response
     */
    public function toBill(Quotation $quotation, Request $request) {
        try {
            $correlative_api = $this->getCorrelativeInvoice(1);
            // return $correlative_api;

            if(!is_numeric($correlative_api)){
                 return [
                     'success' => false,
                     'message' => 'Error al obtener correlativo Api.'
                 ];
            }
            $datoscompany = Company::with('type_regime', 'type_identity_document')->firstOrFail();
            $company = ServiceTenantCompany::firstOrFail();

            $service_invoice = $request->service_invoice;
            $service_invoice['number'] = $correlative_api;
            $service_invoice['date'] = date('Y-m-d');
            $service_invoice['time'] = date('H:i:s');
            if(file_exists(storage_path('sendmail.api')))
                $service_invoice['sendmail'] = true;
            $service_invoice['ivaresponsable'] = $datoscompany->type_regime->name;
            $service_invoice['nombretipodocid'] = $datoscompany->type_identity_document->name;
            $service_invoice['tarifaica'] = $datoscompany->ica_rate;
            $service_invoice['actividadeconomica'] = $datoscompany->economic_activity_code;
            $service_invoice['notes'] = $quotation->observation;
            $service_invoice['payment_form']['payment_form_id'] = 1;
            $service_invoice['payment_form']['payment_method_id'] = 10;
            $service_invoice['payment_form']['payment_due_date'] = date('Y-m-d');
            $service_invoice['payment_form']['duration_measure'] = 0;

            $response =  null;
            $response_status =  null;

            $id_test = $company->test_id;
            $base_url = config('tenant.service_fact');
            if($company->type_environment_id == 2  && $company->test_id != 'no_test_set_id')
                $ch = curl_init("{$base_url}ubl2.1/invoice/{$id_test}");
            else
                $ch = curl_init("{$base_url}ubl2.1/invoice");
            $data_document = json_encode($service_invoice);
            //            return $data_document;
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS,($data_document));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Accept: application/json',
                "Authorization: Bearer {$company->api_token}"
            ));
            $response = curl_exec($ch);
            curl_close($ch);

            $response_model = json_decode($response);
            $error_response_api = $this->validateErrorsResponseApi($response_model);
            $zip_key = null;
            if(!$error_response_api['success']){
                return $error_response_api;
            }
            $invoice_status_api = null;
            //            if(property_exists($response_model, 'urlinvoicepdf') &&  property_exists($response_model, 'urlinvoicexml') )
            //            {
            //                if(is_string($response_model->ResponseDian->Envelope->Body->SendTestSetAsyncResponse->SendTestSetAsyncResult->ZipKey))
            //                {
            //                    $zip_key = $response_model->ResponseDian->Envelope->Body->SendTestSetAsyncResponse->SendTestSetAsyncResult->ZipKey;
            //                }
            //            }
            $response_status = null;
            if($company->type_environment_id == 2  && $company->test_id != 'no_test_set_id'){
                if(array_key_exists('urlinvoicepdf', $response_model) && array_key_exists('urlinvoicexml', $response_model))
                {
                    if(!is_string($response_model->ResponseDian->Envelope->Body->SendTestSetAsyncResponse->SendTestSetAsyncResult->ZipKey))
                    {
                        if(is_string($response_model->ResponseDian->Envelope->Body->SendTestSetAsyncResponse->SendTestSetAsyncResult->ErrorMessageList->XmlParamsResponseTrackId->Success))
                        {
                            if($response_model->ResponseDian->Envelope->Body->SendTestSetAsyncResponse->SendTestSetAsyncResult->ErrorMessageList->XmlParamsResponseTrackId->Success == 'false')
                            {
                                return [
                                    'success' => false,
                                    'message' => $response_model->ResponseDian->Envelope->Body->SendTestSetAsyncResponse->SendTestSetAsyncResult->ErrorMessageList->XmlParamsResponseTrackId->ProcessedMessage
                                ];
                            }
                        }
                    }
                    else
                        if(is_string($response_model->ResponseDian->Envelope->Body->SendTestSetAsyncResponse->SendTestSetAsyncResult->ZipKey))
                        {
                            $zip_key = $response_model->ResponseDian->Envelope->Body->SendTestSetAsyncResponse->SendTestSetAsyncResult->ZipKey;
                        }
                }

                //return $zip_key;

                //declaro variuable response status en null
                $response_status = null;
                //compruebo zip_key para ejecutar servicio de status document

                if($zip_key)
                {
                    //espero 3 segundos para ejecutar sevcio de status document
                    sleep(3);

                    $ch2 = curl_init("{$base_url}ubl2.1/status/zip/{$zip_key}");
                    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "POST");

                    if(file_exists(storage_path('sendmail.api'))){
                        curl_setopt($ch2, CURLOPT_POSTFIELDS, json_encode(array("sendmail" => true)));
                    }
                    curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Accept: application/json',
                        "Authorization: Bearer {$company->api_token}"
                    ));
                    $response_status = curl_exec($ch2);
                    curl_close($ch2);
                    $response_status_decoded = json_decode($response_status);

                    //return $response_status;

                    if($response_status_decoded->ResponseDian->Envelope->Body->GetStatusZipResponse->GetStatusZipResult->DianResponse->IsValid == "true")
                        $this->setStateDocument(1, $correlative_api);
                    //                        assert(true);
                                        else
                                        {
                                            if(is_array($response_status_decoded->ResponseDian->Envelope->Body->GetStatusZipResponse->GetStatusZipResult->DianResponse->ErrorMessage->string))
                                                $mensajeerror = implode(",", $response_status_decoded->ResponseDian->Envelope->Body->GetStatusZipResponse->GetStatusZipResult->DianResponse->ErrorMessage->string);
                                            else
                                                $mensajeerror = $response_status_decoded->ResponseDian->Envelope->Body->GetStatusZipResponse->GetStatusZipResult->DianResponse->ErrorMessage->string;
                                            if($response_status_decoded->ResponseDian->Envelope->Body->GetStatusZipResponse->GetStatusZipResult->DianResponse->IsValid == 'false')
                                            {
                                                return [
                                                    'success' => false,
                                                    'message' => "Error al Validar Factura Nro: {$correlative_api} Errores: ".$mensajeerror
                                                ];
                                            }
                                        }
                                    }
                                    else
                                        return [
                                            'success' => false,
                                            'message' => "Error de ZipKey."
                                        ];
                                }
                                else{
                                    if($response_model->ResponseDian->Envelope->Body->SendBillSyncResponse->SendBillSyncResult->IsValid == "true")
                                        $this->setStateDocument(1, $correlative_api);
                    //                    assert(true);
                else
                {
                    if(is_array($response_model->ResponseDian->Envelope->Body->SendBillSyncResponse->SendBillSyncResult->ErrorMessage->string))
                        $mensajeerror = implode(",", $response_model->ResponseDian->Envelope->Body->SendBillSyncResponse->SendBillSyncResult->ErrorMessage->string);
                    else
                        $mensajeerror = $response_model->ResponseDian->Envelope->Body->SendBillSyncResponse->SendBillSyncResult->ErrorMessage->string;
                    if($response_model->ResponseDian->Envelope->Body->SendBillSyncResponse->SendBillSyncResult->IsValid == 'false')
                    {
                        return [
                            'success' => false,
                            'message' => "Error al Validar Factura Nro: {$correlative_api} Errores: ".$mensajeerror
                       ];
                    }
                }
            }

            DB::connection('tenant')->beginTransaction();
            $nextConsecutive = FacadeDocument::nextConsecutive(1);

            $this->company = Company::query()
                ->with('country', 'version_ubl', 'type_identity_document')
                ->firstOrFail();

            if (($this->company->limit_documents != 0) && (Document::count() >= $this->company->limit_documents)) throw new \Exception("Has excedido el límite de documentos de tu cuenta.");

            $this->document = Document::create([
                'type_document_id' => 1,
                'prefix' => $nextConsecutive->prefix,
                'number' => $correlative_api,
                'type_invoice_id' => 1,
                'client_id' => $quotation->client_id,
                'client' => Client::with('typePerson', 'typeRegime', 'typeIdentityDocument', 'country', 'department', 'city')->findOrFail($quotation->client_id),
                'currency_id' => $quotation->currency_id,
                'date_issue' => Carbon::now(),
                'observation' => $quotation->observation,
                'sale' => $quotation->sale,
                'total_discount' => $quotation->total_discount,
                'taxes' => $quotation->taxes,
                'total_tax' => $quotation->total_tax,
                'subtotal' => $quotation->subtotal,
                'total' => $quotation->total,
                'version_ubl_id' => $quotation->version_ubl_id,
                'ambient_id' => $quotation->ambient_id,

                'payment_form_id' => 1,
                'payment_method_id' =>10,
                'time_days_credit' =>0,

                'response_api' => $response,
                'response_api_status' => $response_status,
                'correlative_api' => $correlative_api
            ]);

            //            $this->document->update([
            //                'xml' => $this->getFileName(),
            //                'cufe' => $this->getCufe()
            //            ]);

            foreach ($quotation->detail_quotations as $item) {
                DetailDocument::create([
                    'document_id' => $this->document->id,
                    'item_id' => $item->item_id,
                    'item' => $item->item,
                    'type_unit_id' => $item->type_unit_id,
                    'quantity' => $item->quantity,
                    'price' => $item->price,
                    'tax_id' => $item->tax_id,
                    'tax' => $item->tax,
                    'total_tax' => $item->total_tax,
                    'subtotal' => $item->subtotal,
                    'discount' => $item->discount,
                    'total' => $item->total
                ]);
            }

           // $this->saveFileAndSignDocument();
            //$this->send();

            $quotation->update([
                'state_quote_id' => 2
            ]);
        }
        catch (\Exception $e) {
            DB::connection('tenant')->rollBack();

            return [
                'success' => false,
                'message' => $e->getMessage()
            ];
        }

        DB::connection('tenant')->commit();

        return [
            'success' => true,
            'message' => "Se registro con éxito el documento #{$this->document->prefix}{$this->document->number}."
        ];

    }

    public function setStateDocument($type_service, $DocumentNumber)
    {
        $company = ServiceTenantCompany::firstOrFail();
        $base_url = config('tenant.service_fact');
        $ch2 = curl_init("{$base_url}ubl2.1/invoice/state_document/{$type_service}/{$DocumentNumber}");

        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Accept: application/json',
            "Authorization: Bearer {$company->api_token}"
        ));
        $response_data = curl_exec($ch2);
        $err = curl_error($ch2);
        curl_close($ch2);
        $response_encode = json_decode($response_data);
        if($err){
            return null;
        }
        else{
            return $response_encode;
        }
    }

    /**
     * Download
     * @param  \App\Models\Tenant\Quotation $quotation
     * @param  Illuminate\Http\Request   $request
     * @return \Illuminate\Http\Response
     */
    public function download(Quotation $quotation, Request $request) {
        $typeIdentityDocuments = TypeIdentityDocument::all();
        $company = Company::firstOrFail();
        $servicecompany = TenantServiceCompany::firstOrFail();
        $document = $quotation;

        $filenameLogo = storage_path("app/public/uploads/logos/{$company->logo}");
		if (file_exists($filenameLogo)) {
            $logoBase64 = base64_encode(file_get_contents($filenameLogo));
            $ext = mime_content_type($filenameLogo);
            $logo = "data:{$ext};base64, {$logoBase64}";
		} else {
            $logo = null;
        }

        $pdf = new PDFHelper('resources/views/pdfs/quotations');
        $pdf->loadCss();
        $pdf->addHeader(view('pdfs.quotations.header', compact("typeIdentityDocuments", "company", "servicecompany", "document", "logo"))->render());
        $pdf->addFooter(view('pdfs.quotations.footer', compact("typeIdentityDocuments", "company", "servicecompany", "document"))->render());
        $pdf->addBody(view('pdfs.quotations.body', compact("typeIdentityDocuments", "company", "servicecompany", "document"))->render());
        return $pdf->download("COT{$quotation->id}.pdf");
        // $pdf = PDF::loadView("pdf/quotation", compact("typeIdentityDocuments", "company", "servicecompany", "document"))->setWarnings(false);
        // return $pdf->download("COT{$quotation->id}.pdf");
    }

    public function getCorrelativeInvoice($type_service)
    {

        $company = ServiceTenantCompany::firstOrFail();
        $base_url = config('tenant.service_fact');
        $ch2 = curl_init("{$base_url}ubl2.1/invoice/current_number/{$type_service}");
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Accept: application/json',
            "Authorization: Bearer {$company->api_token}"
        ));
        $response_data = curl_exec($ch2);
        $err = curl_error($ch2);
        curl_close($ch2);
        $response_encode = json_decode($response_data);
        if($err){
            return null;
        }
        else{
            return $response_encode->number;
        }

    }

    public function email(Quotation $quotation, $email)
    {

        $company = Company::firstOrFail();
        $servicecompany = TenantServiceCompany::firstOrFail();
        $document = $quotation;

        Mail::to($email)->send(new QuotationEmail($company, $document, $servicecompany));

        return [
            'success' => true,
            'message' => "El email se envió correctamente."

        ];


    }

    public function destroy(Quotation $quotation)
    {
        $quotation->delete();

        return [
            'success' => true,
            'message' => 'Registro eliminado correctamente.'
        ];
    }
}
