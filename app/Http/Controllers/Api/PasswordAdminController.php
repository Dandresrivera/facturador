<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\System\User;
use Illuminate\Http\Request;
use App\Http\Requests\Api\PasswordAdminRequest;

class PasswordAdminController extends Controller
{
    public function changeAdminPassword(PasswordAdminRequest $request){
        $file = fopen(storage_path("DEBUG.TXT"), "a+");
        fwrite($file, json_encode($request->password).PHP_EOL);
        fclose($file);

        $user = User::UpdateOrCreate(['email' => $request->email],
                                     ['name' => $request->name,
                                      'password' => bcrypt($request->password)]);
        return [
            'message' => "Se actualizaron/crearon los datos del usuario satisfactoriamente.",
            'success' => true,
            'password' => $request->password,
            'bcrypt(password)' => $user->password
        ];

//        $user = User::where('email', '=', $request->email)->get();
//        if(count($user) > 0){
//            if(isset($request->password) && ($request->password != '')){
//                $password = $request->password;
//                $user->password = bcrypt($password);
//                $user->save();
//            }
//            else{
//                $password = \Str::random(6);
//                $user->password = bcrypt($password);
//                $user->save();
//            }
//        }
//        else{
//            $user = User::create($request->all())
//            if(isset($request->password) && ($request->password != '')){
//                $password = $request->password;
//                $user->password = bcrypt($password);
//                $user->save();
//            }
//            else{
//                $password = \Str::random(6);
//                $user->password = bcrypt($password);
//                $user->save();
//            }
//        }
    }
}
