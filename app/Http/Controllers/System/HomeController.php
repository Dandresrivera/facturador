<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\System\User;
use App\Http\Requests\System\ConfigurationUploadLogoRequest;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        return view('app.system.home');
    }

    public function indexDocument()
    {
        return view('app.system.document');
    }

    public function password()
    {
        return view('app.system.password');
    }

    public function uploadLogo(ConfigurationUploadLogoRequest $request) {
        // $base_url = config('tenant.service_fact');

        $user = User::firstOrFail();
        $file = $request->file('file');

        $name = "logo_admin.{$file->getClientOriginalExtension()}";

        $file->storeAs('public/uploads/logos', $name);

        $user->logo = $name;
        $user->save();

        return [
                'message' => "Se guardaron los cambios.",
                'success' => true
            ];
    }
}
