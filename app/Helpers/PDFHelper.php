<?php

namespace App\Helpers;

use Mpdf\Mpdf;
use Mpdf\HTMLParserMode;
use Mpdf\Output\Destination;
use Mpdf\Config\FontVariables;
use Mpdf\Config\ConfigVariables;

class PDFHelper
{
	/**
	 * Ruta de donde se obtendran los estilos, headers, body y footer del PDF
	 *
	 * @var string
	 */
	protected $basePath;

	/**
	 * Distancia izquierda entre el borde la hoja y el contenido del PDF
	 *
	 * @var int
	 */
	protected $marginLeft;

	/**
	 * Distancia derecha entre el borde la hoja y el contenido del PDF
	 *
	 * @var int
	 */
	protected $marginRight;

	/**
	 * Distancia superior entre el borde la hoja y el contenido del PDF
	 *
	 * @var int
	 */
	protected $marginTop;

	/**
	 * Distancia inferior entre el borde la hoja y el contenido del PDF
	 *
	 * @var int
	 */
	protected $marginBottom;

	/**
	 * Distancia entre el header y el contenido del PDF
	 *
	 * @var int
	 */
	protected $marginHeader;

	/**
	 * Distancia el footer y el contenido del PDF
	 *
	 * @var int
	 */
	protected $marginFooter;

	protected $pdf;

	public function __construct(string $basePath, int $marginLeft = 5, int $marginRight = 5, int $marginTop = 50, int $marginBottom = 5, int $marginHeader = 5, int $marginFooter = 2)
	{
		$this->basePath     = $basePath;
		$this->marginLeft   = $marginLeft;
		$this->marginRight  = $marginRight;
		$this->marginTop    = $marginTop;
		$this->marginBottom = $marginBottom;
		$this->marginHeader = $marginHeader;
		$this->marginFooter = $marginFooter;

		$defaultConfig = (new ConfigVariables())->getDefaults();
		$fontDirs      = $defaultConfig['fontDir'];

		$defaultFontConfig = (new FontVariables())->getDefaults();
		$fontData          = $defaultFontConfig['fontdata'];
		$this->pdf         = new Mpdf([
			'fontDir' => array_merge($fontDirs, [
				base_path('public/fonts/roboto/'),
			]),
			'fontdata' => $fontData + [
				'Roboto' => [
					'R' => 'Roboto-Regular.ttf',
					'B' => 'Roboto-Bold.ttf',
					'I' => 'Roboto-Italic.ttf',
				]
			],
			'default_font'  => 'Roboto',
			'margin_left'   => $this->marginLeft,
			'margin_right'  => $this->marginRight,
			'margin_top'    => $this->marginTop,
			'margin_bottom' => $this->marginBottom,
			'margin_header' => $this->marginHeader,
			'margin_footer' => $this->marginFooter
		]);
	}

	/**
	 * Función para cargar los estilos del PDF
	 *
	 * @param string $path Nombre y ruta absoluta de la hoja de estilos, si no se ingresa se tomará en cuenta la ruta ingresada en el constructor
	 */
	public function loadCss(string $path = null)
	{
		$stylePath = $this->basePath . '/styles.css';
		if ($path) {
			$stylePath = $path;
		}
		$this->pdf->WriteHTML(file_get_contents(base_path($stylePath)), HTMLParserMode::HEADER_CSS);
    }

    public function addHeader(string $header)
    {
        $this->pdf->SetHTMLHeader($header);
    }

    public function addFooter(string $footer)
    {

        $this->pdf->SetHTMLFooter($footer);
    }

    public function addBody(string $body)
    {
        $this->pdf->WriteHTML($body, HTMLParserMode::HTML_BODY);
    }

    public function store(string $filename)
    {
        $this->pdf->Output($filename);
    }

    public function download(string $filename)
    {
        $this->pdf->Output($filename, Destination::DOWNLOAD);
    }
}
