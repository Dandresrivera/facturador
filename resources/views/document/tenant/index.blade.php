@extends('layouts.app')

@section('content')
    <tenant-document-document api="{{ config('tenant.service_fact') }}" route="{{route('tenant.document')}}"></tenant-document-document>
@endsection
