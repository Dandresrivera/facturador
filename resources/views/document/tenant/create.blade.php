@extends('layouts.app')

@section('content')
    <tenant-document-form route="{{route('tenant.document.form')}}" uuid="{{ $uuid }}"></tenant-document-form>
@endsection
