<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width"/>
    {{-- <title>COTIZACION Nro: COT-{{$document->id}}</title> --}}
</head>

<body>
    <table style="width: 100%">
        <tbody>
            <tr>
                <td style="width: 90px;">
                    <p>
                    <strong style="font-weight: bold;">CC o NIT</strong><br>
                    Cliente<br>
                    Regimen<br>
                    Dirección<br>
                    Ciudad<br>
                    Telefono<br>
                    E-mail<br>
                    @if($document->seller)
                        Vendedor<br>
                    @endif
                    </p>
                </td>
                <td style="width: 10px;">
                    <p>:</p>
                    <p>:</p>
                    <p>:</p>
                    <p>:</p>
                    <p>:</p>
                    <p>:</p>
                    <p>:</p>
                    @if($document->seller)
                    <p>:</p>
                    @endif
                </td>
                <td>
                    <p>
                    <strong style="font-weight: bold;">{{$document->client->identification_number}}-{{$document->client->dv ?? NULL}}</strong><br>
                            {{$document->client->name}}<br>
                            @inject('regimen', 'App\Models\Tenant\TypeRegime')
                            {{$regimen->findOrFail($document->client->type_regime_id)->name}}<br>
                            {{$document->client->address}}<br>
                            @inject('cliente', 'App\Models\Tenant\Client')
                            @inject('ciudad', 'App\Models\Tenant\City')
                            @inject('pais', 'App\Models\Tenant\Country')
                            {{$ciudad->findOrFail($cliente->findOrFail($document->client->id)->city_id)->name}} - {{$pais->findOrFail($cliente->findOrFail($document->client->id)->country_id)->name}}<br>
                            {{$document->client->phone}}<br>
                            {{$document->client->email}}<br>
                            @if($document->seller)
                                {{$document->seller}}<br>
                            @endif
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
    <br/>
    <br/>

        <table class="table" style="width: 100%;">
            <thead>
                <tr>
                    <th class="text-center">Código</th>
                    <th class="text-center">Descripcion</th>
                    <th class="text-center">Cantidad</th>
                    <th class="text-center">UM</th>
                    <th class="text-center">Val. Unit</th>
                    <th class="text-center">IVA/IC</th>
                    <th class="text-center">Dcto</th>
                    <th class="text-center">Val. Item</th>
                </tr>
            </thead>
            <tbody>
                @foreach($document->detail_quotations as $item)
                    <tr>
                        <td>{{$item->item->code}}</td>
                        <td>{{$item->item->name}}
                            @if(isset($item->item->description))
                                <br><span style="font-size: 8px"> {{ $item->item->description }}</span>
                            @endif
                        </td>
                        <td class="text-right">{{number_format($item->quantity, 2)}}</td>
                        <td class="text-right">{{$item->item->type_unit->name}}</td>
                        <td class="text-right">{{number_format($item->price, 2)}}</td>
                        <td class="text-right">{{number_format($item->total_tax, 2)}}</td>
                        <td class="text-right">{{number_format($item->discount, 2)}}</td>
                        <td class="text-right">{{number_format($item->total, 2)}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <br><br>

        <table class="table" style="width: 100%;">
            <thead>
                <tr>
                    <th class="text-center">Impuestos</th>
                    <th class="text-center">Retenciones</th>
                    <th class="text-center">Totales</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="width: 33.33%;">
                        <table class="table" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th class="text-center">Tipo</th>
                                    <th class="text-center">Base</th>
                                    <th class="text-center">Porcentaje</th>
                                    <th class="text-center">Valor</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($document->taxes))
                                    <?php $TotalImpuestos = 0; ?>
                                    @foreach($document->taxes as $item)
                                        @if ($item->total > 0)
                                            <?php $TotalImpuestos = $TotalImpuestos + $item->total ?>
                                            <tr>
                                                <td>{{$item->type_tax->name}}</td>
                                                <td>{{number_format($item->total/($item->rate/100), 2)}}</td>
                                                <td>{{$item->rate}}%</td>
                                                <td>{{number_format($item->total, 2)}}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </td>
                    <td style="width: 33.33%;">
                        <table class="table" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th class="text-center">Tipo</th>
                                    <th class="text-center">Base</th>
                                    <th class="text-center">Porcentaje</th>
                                    <th class="text-center">Valor</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($document->taxes))
                                    <?php $TotalRetenciones = 0; ?>
                                    @foreach($document->taxes as $item)
                                        @if ($item->apply)
                                            <?php $TotalRetenciones = $TotalRetenciones + $item->retention ?>
                                            <tr>
                                                <td>{{$item->type_tax->name}}</td>
                                                <td>{{number_format($item->retention/($item->rate/100), 2)}}</td>
                                                <td>{{$item->rate}}%</td>
                                                <td>{{number_format($item->retention, 2)}}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </td>
                    <td style="width: 33.33%;">
                        <table class="table" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th class="text-center">Concepto</th>
                                    <th class="text-center">Valor</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Base:</td>
                                    <td>{{number_format($document->sale, 2)}}</td>
                                </tr>
                                <tr>
                                    <td>Impuestos:</td>
                                    <td>{{number_format($TotalImpuestos, 2)}}</td>
                                </tr>
                                <tr>
                                    <td>Retenciones:</td>
                                    <td>{{number_format($TotalRetenciones, 2)}}</td>
                                </tr>
                                <tr>
                                    <td>Dctos (-):</td>
                                    <td>{{number_format($document->total_discount, 2)}}</td>
                                </tr>
                                <tr>
                                    <td>Total Factura:</td>
                                    <td>{{number_format($document->total, 2)}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>

        <div class="summarys">
            <div class="text-word" id="note">
                @inject('Varios', 'App\Custom\Various')
                <h5>NOTAS:</h5>
                <p style="font-size: 12px;">{{$document->observation}} </p>
                <br>
                <p style="font-size: 12px;"><strong style="font-weight: bold;">SON</strong>: {{$Varios->NumerosALetras($document->total)}} M/CTE*********.</p>
            </div>
        </div>

        <div class="summary" >
            <div class="class="text-word"" id="note">
                <p>CUALQUIER INQUIETUD ACERCA DE ESTE DOCUMENTO, COMUNICARSE AL TELEFONO {{$servicecompany->phone}} o al e-mail {{$company->email}}<br>
                    {{-- <br>
                    <div id="firma">
                        <p><strong>FIRMA ACEPTACIÓN:</strong></p><br>
                        <p><strong>CC:</strong></p><br>
                        <p><strong>FECHA:</strong></p><br>
                    </div> --}}
                </p>
            </div>
        </div>
</body>
</html>
