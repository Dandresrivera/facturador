<table style="width: 100%;" id="header">
    <tbody>
        <tr>
            <td style="width: 30%; text-align: center;">
                <p style="font-size: 14px; font-weight: bold">COTIZACION NRO</p>
                <p style="color: red;
                    font-weight: bold;
                    font-size: 16px;
                    padding: 20px 5px;">COT-{{$document->id}}</p>
                <p style="font-size: 12px;">Fecha: {{Carbon\Carbon::parse($document->date_issue)->format('Y-m-d')}}<br>
                    Hora: {{Carbon\Carbon::parse($document->date_issue)->format('H:m:s')}}</p>
            </td>
            <td style="width: 40%; text-align: center;">
                <strong>{{$company->name}}</strong><br>
                <div id="empresa-header1">
                    NIT: {{$servicecompany->identification_number}} - {{$servicecompany->type_regime->name}} - {{$company->type_regime->name}}<br>
                    TARIFA ICA: {{$company->ica_rate}}% - ACTIVIDAD ECONOMICA: {{$company->economic_activity_code}}<br>
                    REPRESENTACION GRAFICA DE COTIZACION DE PRODUCTOS Y SERVICIOS<br>
                    {{$servicecompany->address}} - {{$servicecompany->municipality->name}} - {{$servicecompany->country->name}} Telefono - {{$servicecompany->phone}}<br>
                    E-mail: {{$company->email}}<br>
                </div>
            </td>
            <td style="width: 30%; text-align: right;">
                <img style="width: 140px; height: auto;" src="{{ $logo }}" alt="Logo">
            </td>
        </tr>
    </tbody>
</table>
