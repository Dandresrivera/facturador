<hr style="margin-bottom: 4px;">
<table style="width: 100%;">
    <tbody>
        <tr>
            <td style="width: 70%;">
                <p style="font-size: 12px;">Cotizacion No: {{$document->id}} - Fecha: {{Carbon\Carbon::parse($document->date_issue)->format('Y-m-d')}}</p>
            </td>
            <td style="width: 30%; text-align: right">
                <p style="font-size: 12px;">Página {PAGENO} de {nb}</p>
            </td>
        </tr>
    </tbody>
</table>
