@extends('layouts.app')

@section('content')
	@php
		if (app(Hyn\Tenancy\Contracts\CurrentHostname::class)){
			$company = \App\Models\Tenant\Company::firstOrFail();
		} else {
			$company = \App\Models\System\User::firstOrFail();
		}
    @endphp
    <auth-login path_logo="{{($company->logo != null) ? "/storage/uploads/logos/{$company->logo}" : 'assets/images/logo_bee.png'}}"></auth-login>
@endsection
