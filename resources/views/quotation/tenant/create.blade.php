@extends('layouts.app')

@section('content')
    <tenant-quotation-form  api="{{ config('tenant.service_fact') }}" route="{{route('tenant.quotation.form')}}"></tenant-quotation-form>
@endsection
