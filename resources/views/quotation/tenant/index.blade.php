@extends('layouts.app')

@section('content')
    <tenant-quotation-quotation api="{{ config('tenant.service_fact') }}" route="{{route('tenant.quotation')}}"></tenant-quotation-quotation>
@endsection
