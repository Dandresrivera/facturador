@php
    $path = explode('/', request()->path());
    $path[1] = (array_key_exists(1, $path)> 0)?$path[1]:'';
    $path[2] = (array_key_exists(2, $path)> 0)?$path[2]:'';
    $path[0] = ($path[0] === '')?'companies':$path[0];
@endphp

<aside class="sidebar" style="display: block;">
    <system-logo url="/companies" path_logo="{{(auth()->user()->logo != null) ? "/storage/uploads/logos/".auth()->user()->logo : ''}}" ></system-logo>
    {{-- <a href="{{route('companies')}}" class="logo">
        <img src="{{asset('assets/images/favicon_dian.ico')}}" alt="O"> <b>Bee</b>
    </a> --}}
    <nav>
        <ul class="sidebar-list">
            <li>
                <a class="{{ ($path[0] === 'companies' && $path[1] === '')?'sidebar-list-item active':'sidebar-list-item' }}" href="{{route('companies')}}">
                    <i class="far fa-building"></i>
                    <span>Compañias</span>
                </a>
            </li>
            <li>
                <a class="{{ ($path[0] === 'companies' && $path[1] === 'document')?'sidebar-list-item active':'sidebar-list-item' }}" href="{{route('documents')}}">
                    <i class="far fa-file-alt"></i>
                    <span>Documentos</span>
                </a>
            </li>
            <li>
                <a class="{{ ($path[0] === 'companies' && $path[1] === 'password')?'sidebar-list-item active':'sidebar-list-item' }}" href="{{route('password')}}">
                    <i class="fas fa-key"></i>
                    <span>Password</span>
                </a>
            </li>
            <li>
                <a class="{{ ($path[0] === 'logs' )?'sidebar-list-item active':'sidebar-list-item' }}" href="{{route('logs')}}" target="_BLANK">
                    <i class="fas fa-bug"></i>
                    <span>Logs</span>
                </a>
            </li>
        </ul>
    </nav>
</aside>
