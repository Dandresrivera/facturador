<header class="header">
    <ul class="header-nav">
        <li>
            <button id="js-toggle-sidebar" class="header-nav-item mr-20">
                <i class="fas fa-bars"></i>
            </button>
        </li>
    </ul>
    <h2>{{auth()->user()->name}}</h2>

    <ul class="header-nav pull-right">
        {{-- <notification-notification :user="{{auth()->user()}}"></notification-notification> --}}
        <menu-popover :user="{{auth()->user()}}"></menu-popover>
    </ul>
</header>
