@extends('layouts.app')

@section('content')
	@php
        $company = \App\Models\Tenant\Company::firstOrFail();
    @endphp
    <tenant-configuration-configuration route="{{route('tenant.configuration')}}" path_logo="{{($company->logo != null) ? "/storage/uploads/logos/{$company->logo}" : ''}}"></tenant-configuration-configuration>
@endsection
