<?php

$currentHostname = app(Hyn\Tenancy\Contracts\CurrentHostname::class);

/*
|--------------------------------------------------------------------------
| Auth:Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "auth" middleware group. Now create something great!
|
*/

if ($currentHostname == null) {
    Route::domain(config('tenant.app_url_base'))->group(function() {
        Route::middleware('auth:admin')->group(function() {
            Route::get('/', function () {
                return redirect()->route('companies');
            });

            Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->name('logs');

            Route::get('/companies', 'System\HomeController@index')->name('companies');
            Route::get('/companies/document', 'System\HomeController@indexDocument')->name('documents');
            Route::get('/companies/password', 'System\HomeController@password')->name('password');
            Route::post('/companies/logo', 'System\HomeController@uploadLogo');


            Route::prefix('/system')->group(function() {
                Route::post('/company', 'System\CompanyController@store')->name('system.company');
                Route::post('/companyAll', 'System\CompanyController@all');
                Route::put('/company/{company}', 'System\CompanyController@update');
                Route::delete('/company/{company}', 'System\CompanyController@destroy');
                Route::get('/companyTables', 'System\CompanyController@tables')->name('system.tables');
                Route::get('/companyCascade', 'System\CompanyController@cascade')->name('system.cascade');
                Route::get('/company/informationDocument/{nit}', 'System\CompanyController@getInformationDocument')->name('system.information_document');
                Route::get('/company/informationDocument/{nit}/{desde}', 'System\CompanyController@getInformationDocument')->name('system.information_document');
                Route::get('/company/informationDocument/{nit}/{desde}/{hasta}', 'System\CompanyController@getInformationDocument')->name('system.information_document');
                Route::post('/company/change-user-data', 'Api\PasswordAdminController@changeAdminPassword')->name('change-user-data');
            });
        });
    });
}
