# Facturación Electrónica Colombia

[![Build Status](https://img.shields.io/badge/stable-1.0-brightgreen.svg)](https://gitlab.com/Stenfrank/factura) [![Build Status](https://img.shields.io/badge/licence-MIT-blue.svg)](https://opensource.org/licenses/MIT) [![Build Status](https://img.shields.io/badge/laravel-5.7-orange.svg)](https://laravel.com/docs/5.7)

## Acerca de FACTURALATAM

Estamos enfocados en crear herramientas y soluciones para desarrolladores de software, empresas medianas y pequeñas, tenemos un sueño, queremos democratizar la facturación electrónica.

## Instalación del Facturador Pro

Para conocer el proceso de instalación del facturador, visite la documentación

[Nuevo - Video instalación y configuración] (https://youtu.be/D5aMkQy9UrM)

[Manual - API - Facturador] (https://drive.google.com/file/d/1xSmE42ReriFGA0nfKT50d_o7T_DP3kBC/view?usp=sharing "Clic")

[Manual - API - Facturador - Una sola instancia - Hetzner] (https://docs.google.com/document/d/1hz3W9dn7HJJD9NSfo6zc4jjidQoPtSJZmoljUbqIy94/edit "Clic")

[Manual - Forge](https://docs.google.com/document/d/1NVV2yv8ys8tjdz5ByvEPhIUdm0GJ95XiB-iMlElaJTA/edit# "Clic")

[Manual - Windows - Linux](https://drive.google.com/open?id=1Jf0vgGvx27MbOB4JYsk9Gzgd5QIl32j35pwU1LI_Woo "Manual - Windows - Linux")

[Manual - Docker - Linux](https://docs.google.com/document/d/1E8jOrnbASjzBqhvgjhlJdhHExUjq4A0DoRKhkFEuFyY/edit?usp=sharing "Clic")

[Manual - Docker - Linux - Hetzner](https://docs.google.com/document/d/18Ko-pMPJB5kG-pR3hksJZWB9nJT0y-ZZicRbOwTTS2Y/edit "Clic")



## Instalación/Actualización SSL Facturador Pro - Docker

[Manual](https://docs.google.com/document/d/1CrUltE6EIfdWfMoKn5DV_NcCVRB__f3rv_AEQirTu-I/edit#heading=h.x5brmjfcp3dc "Clic") 

## Cuadro de datos de la empresa (inicio de configuración)

[Manual](https://drive.google.com/file/d/1FqbFM6dkU-9G1SYElBETzHNll8nxYgbm/view?usp=sharing "Clic")

## Pasar a Producción

[Manual](https://docs.google.com/document/d/1OtfSwII4unP-scJRFy5KqJiL7oZZZT5Qo8ALhD2PwFY/edit?usp=sharing "Clic")

## Actualización Docker-Git

Visite el siguiente enlace: [Guía](https://docs.google.com/document/d/11PI1a9yjCPfH9CCuWmJSrdj1V8IEUffqurqvdkw29co/edit?usp=sharing "Guía")
 
## Sponsors

 - **[FACTURALATAM](http://facturalatam.com/)** 
 - **[FACTURALOPERU](http://facturaloperu.com/)** 
 - **[CursosDev](http://cursosdev.com/)** 

## Contribución
 
Gracias por considerar contribuir al Facturador Pro

## Vulnerabilidades de seguridad

Si descubre una vulnerabilidad de seguridad, envíe un correo electrónico a facturalatamglobal@gmail.com. Las vulnerabilidades de seguridad serán tratadas con prontitud.



