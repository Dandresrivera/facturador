<?php

return [

    'force_https' => env('FORCE_HTTPS', false),
    'service_fact' => env('SERVICE_FACT', 'http://35.239.249.101/api/'),
    'app_url_base' => env('APP_URL_BASE'),
    'db_database' => env('DB_DATABASE'),

];
